package org.msh.fdt.dto;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by kenny on 4/2/14.
 */

public class Account {
    private int id;
    private String name;
    private String description;
    private int accountTypeId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(int accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

}
