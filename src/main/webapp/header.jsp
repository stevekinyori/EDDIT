<%--
  Created by IntelliJ IDEA.
  User: kenny
  Date: 3/20/14
  Time: 8:08 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="row">
    <div class="logo">
        <img src="images/gok.png"/>
    </div>
    <div class="facilityName" style="margin-bottom: 10px">
        <label style="font-size: 14px;">Ministry of Health</label>
    </div>
    <div class="facilityName">
        <label><%= session.getAttribute("facility_name") %></label>
    </div>

    <div class="menubar">


        <!--<div class="menubar_item" onclick="">
            Send data to DHIS
        </div> -->


        <div class="menubar_item" onclick="window.location='home.jsp'">
            Home
        </div>
        <div class="menubar_item" onclick="window.location='transactions.jsp'">
            Transactions
        </div>
        <% if(session.getAttribute("mod_reports") != null) { %>
        <div class="menubar_item" onclick="window.location='reports.jsp'">
            Reports
        </div>
        <% }
            if(session.getAttribute("mod_settings") != null) {
        %>
        <div class="menubar_item" onclick="window.location='settings.jsp'">
            Settings
        </div>
        <% }
            if(session.getAttribute("mod_admin") != null) { %>
        <div class="menubar_item" onclick="window.location='admin.jsp'">
            Admin
        </div>
        <% } %>
        <div class="menubar_item" onclick="logout()">
            Logout
        </div>
        <div style="position: relative;left: 60%;">
           Logged in as: <%= session.getAttribute("username") %>
            <br>
            Store: <%= session.getAttribute("account_name") %>
        </div>
    </div>


</div>
